# Kartka na swieta Bozego Narodzenia.

### Projekt wykonany w aplikacji Clion firmy JetBrains.


1. #### Informacje techniczne.

* Pliki SFML oraz MinGW użyte podczas tworzenia pobrane zostały ze strony: [Kliknij w link do strony z SFML 2.5.1](<https://www.sfml-dev.org/download/sfml/2.5.1/>).

![Windows 10 C disc](screenshots/SFML_and_MinGW_version.png)

* Dla systemu Windows 10 wypakowane zostały w następującej lokalizacji.

![Windows 10 C disc](screenshots/SFML_and_MinGW_location.png)

* Dodane zostały również zmienne globalne zgodnei z tym co widać na poniższym zrzucie.

![Windows 10 C disc](screenshots/SFML_and_MinGW_enviroment_variables_win.png)

* Pliki audio oraz czcionek wczytywane przez program znajdują się w folderze "cmake-build-debug".
Do w.w. folderu dodano także plik z openal32.dll żeby była możliwość podlinkowania dynamicznie obsługi dźwięku. 
(Brak tego pliku powodował błędy w kompilacji kodu obsługującego muzykę )


2. #### Podgląd wyglądu kartki w systemie Linux.

![Christmas card](screenshots/christmas_card.png)