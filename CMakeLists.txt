cmake_minimum_required(VERSION 3.21)
project(christmas_card)

set(CMAKE_CXX_STANDARD 11)
add_compile_options(-Wall -Wextra)

add_executable(christmas_card main.cpp)

if(UNIX)
    include_directories ("${PROJECT_SOURCE_DIR}/linux_SFML-2.5.1/include")
    set(SFML_DIR "${PROJECT_SOURCE_DIR}/linux_SFML-2.5.1/lib/cmake/SFML/")
    elseif(WIN32)
        set(SFML_STATIC_LIBRARIES TRUE)
        set(SFML_DIR C:/SFML/lib/cmake/SFML)
        include_directories(C:/SFML/include)
endif()

find_package(SFML COMPONENTS system window graphics audio network REQUIRED)
target_link_libraries(christmas_card sfml-system sfml-window sfml-graphics sfml-audio)