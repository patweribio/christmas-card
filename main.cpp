#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>



int main() {

    //smoothen edges
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

    // create the window
    sf::RenderWindow window(sf::VideoMode(1000, 600), "Christmas card", sf::Style::Default, settings);

    //define colors
    sf::Color BROWN (139,69,19);
    sf::Color TREE_GREEN (0,102,0);
    sf::Color YELLOW_STAR (255,255,0);
    sf::Color LIGHT_YELLOW (255,255,204);
    sf::Color NIGHT_SKY (15,15,61);
    sf::Color WHITE (255,255,255);
    sf::Color PINK1 (255,102,102);
    sf::Color PINK2 (192,102,102);
    sf::Color RED_WINE (180,0,0);
    sf::Color BORDO (131,0,51);
    sf::Color PINK3 (178,21,255);
    sf::Color ORANGE (245, 137,0);
    sf::Color PLUM (167,151,250);
    sf::Color AQUA (46,255,204);
    sf::Color GRAY (109,145,137);
    sf::Color GRAY2 (109,129,157);
    sf::Color PURPLE (129,89,116);
    sf::Color PURPLE2 (129,67,141);
    sf::Color BROWN2 (167,151,92);

    //rectangle (prostokat)
    sf::RectangleShape rectangle;

    //tetragon (czworokat)
    sf::ConvexShape convex;
    convex.setPointCount(4);        //resize

    //circle
    sf::CircleShape circle;

    //ellipse
    sf::CircleShape ellipse;

    //triangle
    sf::VertexArray triangle(sf::Triangles, 3);

    //line
    sf::RectangleShape line;

    //font
    sf::Font font;
    font.loadFromFile("Amadeus.ttf");

    //text
    sf::Text text;
    text.setFont(font);

    //music
    sf::Music music;
    if (!music.openFromFile("Jingle_Bells.ogg"))
        return -1; // error
    music.setVolume(30);
    music.play();
    music.setLoop(true);


    // run the program as long as the window is open
    while (window.isOpen())
    {
        // check all the window's events that were triggered since the last iteration of the loop

        sf::Event event;
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // clear the window with black color
        window.clear(sf::Color::Black);

        //heaven
        rectangle.setSize(sf::Vector2f (1000.f,350.f));
        rectangle.setPosition (0.f,0.f);
        rectangle.setFillColor(NIGHT_SKY);
        window.draw(rectangle);

        //snow on the ground
        rectangle.setSize(sf::Vector2f (1000.f,250.f));
        rectangle.setPosition(sf::Vector2f (0.f,350.f));
        rectangle.setFillColor(WHITE);
        window.draw(rectangle);

        //christmas tree
        triangle[0].color = triangle[1].color = triangle[2].color = TREE_GREEN;
        triangle[0].position = sf::Vector2f(800.f, 100.f);
        triangle[1].position = sf::Vector2f(700.f, 300.f);
        triangle[2].position = sf::Vector2f(900.f, 300.f);
        window.draw(triangle);
        triangle[0].position = sf::Vector2f(800.f, 160.f);
        triangle[1].position = sf::Vector2f(670.f, 440.f);
        triangle[2].position = sf::Vector2f(930.f, 440.f);
        window.draw(triangle);

        //tree trunk
        rectangle.setSize(sf::Vector2f (40.f,30.f));
        rectangle.setPosition(780.f, 440.f);
        rectangle.setOutlineThickness(0.f);
        rectangle.setFillColor(BROWN) ;
        window.draw(rectangle);

        //star on the tree
        triangle[0].color = triangle[1].color = triangle[2].color = YELLOW_STAR;
        triangle[0].position = sf::Vector2f(800.f, 85.f);
        triangle[1].position = sf::Vector2f(780.f, 110.f);
        triangle[2].position = sf::Vector2f(820.f, 110.f);
        window.draw(triangle);
        triangle[0].position = sf::Vector2f(800.f, 120.f);
        triangle[1].position = sf::Vector2f(780.f, 95.f);
        triangle[2].position = sf::Vector2f(820.f, 95.f);
        window.draw(triangle);

        //gift number 1
        rectangle.setSize(sf::Vector2f(60.f, 40.f));
        rectangle.setPosition (675.f,500.f);
        rectangle.setFillColor(PINK1);
        rectangle.setOutlineThickness(1.f);
        rectangle.setOutlineColor(PINK2);
        window.draw(rectangle);

        convex.setPoint(0, sf::Vector2f(695.f, 480.f));
        convex.setPoint(1, sf::Vector2f(755.f, 480.f));
        convex.setPoint(2, sf::Vector2f(735.f, 500.f));
        convex.setPoint(3, sf::Vector2f(675.f, 500.f));
        convex.setFillColor(PINK1);
        convex.setOutlineThickness(1.f);
        convex.setOutlineColor(PINK2);
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(735.f, 500.f));
        convex.setPoint(1, sf::Vector2f(755.f, 480.f));
        convex.setPoint(2, sf::Vector2f(755.f, 520.f));
        convex.setPoint(3, sf::Vector2f(735.f, 540.f));
        convex.setFillColor(PINK1);
        convex.setOutlineThickness(1.f);
        convex.setOutlineColor(PINK2);
        window.draw(convex);

        //gift number 2
        rectangle.setSize(sf::Vector2f(60.f, 40.f));
        rectangle.setPosition (875.f,480.f);
        rectangle.setFillColor(GRAY);
        rectangle.setOutlineThickness(1.f);
        rectangle.setOutlineColor(GRAY2);
        window.draw(rectangle);

        convex.setPoint(0, sf::Vector2f(895.f, 460.f));
        convex.setPoint(1, sf::Vector2f(955.f, 460.f));
        convex.setPoint(2, sf::Vector2f(935.f, 480.f));
        convex.setPoint(3, sf::Vector2f(875.f, 480.f));
        convex.setFillColor(GRAY);
        convex.setOutlineThickness(1.f);
        convex.setOutlineColor(GRAY2);
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(935.f, 480.f));
        convex.setPoint(1, sf::Vector2f(955.f, 460.f));
        convex.setPoint(2, sf::Vector2f(955.f, 500.f));
        convex.setPoint(3, sf::Vector2f(935.f, 520.f));
        convex.setFillColor(GRAY);
        convex.setOutlineThickness(1.f);
        convex.setOutlineColor(GRAY2);
        window.draw(convex);

        //gift number 3
        rectangle.setSize(sf::Vector2f(60.f, 40.f));
        rectangle.setPosition (805.f,530.f);
        rectangle.setFillColor(PURPLE);
        rectangle.setOutlineThickness(1.f);
        rectangle.setOutlineColor(PURPLE2);
        window.draw(rectangle);

        convex.setPoint(0, sf::Vector2f(825.f, 510.f));
        convex.setPoint(1, sf::Vector2f(885.f, 510.f));
        convex.setPoint(2, sf::Vector2f(865.f, 530.f));
        convex.setPoint(3, sf::Vector2f(805.f, 530.f));
        convex.setFillColor(PURPLE);
        convex.setOutlineThickness(1.f);
        convex.setOutlineColor(PURPLE2);
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(865.f, 530.f));
        convex.setPoint(1, sf::Vector2f(885.f, 510.f));
        convex.setPoint(2, sf::Vector2f(885.f, 550.f));
        convex.setPoint(3, sf::Vector2f(865.f, 570.f));
        convex.setFillColor(PURPLE);
        convex.setOutlineThickness(1.f);
        convex.setOutlineColor(PURPLE2);
        window.draw(convex);

        //gift number 1 ribbon
        line.setFillColor(RED_WINE);
        line.setPosition (sf::Vector2f(710.f,500.f));
        line.setSize(sf::Vector2f(40.f,10.f));
        line.setRotation(90.f);
        window.draw (line);

        convex.setPoint(0, sf::Vector2f(710.f, 500.f));
        convex.setPoint(1, sf::Vector2f(700.f, 500.f));
        convex.setPoint(2, sf::Vector2f(720.f, 480.f));
        convex.setPoint(3, sf::Vector2f(730.f, 480.f));
        convex.setFillColor(RED_WINE);
        convex.setOutlineThickness(0.f);
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(688.f, 486.f));
        convex.setPoint(1, sf::Vector2f(750.f, 486.f));
        convex.setPoint(2, sf::Vector2f(742.f, 491.f));
        convex.setPoint(3, sf::Vector2f(682.f, 491.f));
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(742.f, 491.f));
        convex.setPoint(1, sf::Vector2f(749.f, 486.f));
        convex.setPoint(2, sf::Vector2f(749.f, 528.f));
        convex.setPoint(3, sf::Vector2f(742.f, 533.f));
        window.draw(convex);

        //gift number 2 ribbon
        line.setFillColor(RED_WINE);
        line.setPosition (sf::Vector2f(910.f,480.f));
        line.setSize(sf::Vector2f(40.f,10.f));
        line.setRotation(90.f);
        window.draw (line);

        convex.setPoint(0, sf::Vector2f(910.f, 480.f));
        convex.setPoint(1, sf::Vector2f(900.f, 480.f));
        convex.setPoint(2, sf::Vector2f(920.f, 460.f));
        convex.setPoint(3, sf::Vector2f(930.f, 460.f));
        convex.setFillColor(RED_WINE);
        convex.setOutlineThickness(0.f);
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(888.f, 466.f));
        convex.setPoint(1, sf::Vector2f(950.f, 466.f));
        convex.setPoint(2, sf::Vector2f(942.f, 471.f));
        convex.setPoint(3, sf::Vector2f(882.f, 471.f));
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(942.f, 471.f));
        convex.setPoint(1, sf::Vector2f(949.f, 466.f));
        convex.setPoint(2, sf::Vector2f(949.f, 508.f));
        convex.setPoint(3, sf::Vector2f(942.f, 513.f));
        window.draw(convex);

        //gift number 3 ribbon
        line.setFillColor(RED_WINE);
        line.setPosition (sf::Vector2f(840.f,530.f));
        line.setSize(sf::Vector2f(40.f,10.f));
        line.setRotation(90.f);
        window.draw (line);

        convex.setPoint(0, sf::Vector2f(840.f, 530.f));
        convex.setPoint(1, sf::Vector2f(830.f, 530.f));
        convex.setPoint(2, sf::Vector2f(850.f, 510.f));
        convex.setPoint(3, sf::Vector2f(860.f, 510.f));
        convex.setFillColor(RED_WINE);
        convex.setOutlineThickness(0.f);
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(818.f, 516.f));
        convex.setPoint(1, sf::Vector2f(880.f, 516.f));
        convex.setPoint(2, sf::Vector2f(872.f, 521.f));
        convex.setPoint(3, sf::Vector2f(812.f, 521.f));
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(872.f, 521.f));
        convex.setPoint(1, sf::Vector2f(879.f, 516.f));
        convex.setPoint(2, sf::Vector2f(879.f, 558.f));
        convex.setPoint(3, sf::Vector2f(872.f, 563.f));
        window.draw(convex);

        //garland on the tree (girlandy)
        line.setFillColor(PINK2);
        line.setPosition (sf::Vector2f(758.f,180.f));
        line.setSize(sf::Vector2f(135.f,3.f));
        line.setRotation(30.f);
        window.draw (line);

        line.setPosition (sf::Vector2f(877.f,250.f));
        line.setSize(sf::Vector2f(215.f,3.f));
        line.setRotation(145.f);
        window.draw (line);

        line.setPosition (sf::Vector2f(701.f,370.f));
        line.setSize(sf::Vector2f(220.f,3.f));
        line.setRotation(10.f);
        window.draw (line);

        //christmas balls
        circle.setRadius(15.f);

        circle.setFillColor(PINK3);
        circle.setPosition(sf::Vector2f(790.f, 155.f));
        window.draw(circle);

        circle.setFillColor(ORANGE);
        circle.setPosition(sf::Vector2f(705.f, 390.f));
        window.draw(circle);

        circle.setFillColor(GRAY2);
        circle.setPosition(sf::Vector2f(805.f, 400.f));
        window.draw(circle);

        circle.setFillColor(PLUM);
        circle.setPosition(sf::Vector2f(746.f, 205.f));
        window.draw(circle);

        circle.setFillColor(ORANGE);
        circle.setPosition(sf::Vector2f(795.f, 240.f));
        window.draw(circle);

        circle.setFillColor(BROWN2);
        circle.setPosition(sf::Vector2f(827.f, 290.f));
        window.draw(circle);

        circle.setFillColor(LIGHT_YELLOW);
        circle.setPosition(sf::Vector2f(770.f, 340.f));
        window.draw(circle);

        circle.setFillColor(BORDO);
        circle.setPosition(sf::Vector2f(843.f, 355.f));
        window.draw(circle);

        circle.setFillColor(AQUA);
        circle.setPosition(sf::Vector2f(745.f, 275.f));
        window.draw(circle);

        //bear
        //---------------- left leg
        line.setFillColor(BROWN);
        line.setPosition (sf::Vector2f(187.f,490.f));
        line.setSize(sf::Vector2f(100.f,60.f));
        line.setRotation(150.f);
        window.draw (line);

        //--------------- right leg
        line.setFillColor(BROWN);
        line.setPosition (sf::Vector2f(317.f,420.f));
        line.setSize(sf::Vector2f(100.f,60.f));
        line.setRotation(40.f);
        window.draw (line);

        //---------------- body
        ellipse.setRadius(25.f);
        ellipse.setScale(3,4);
        ellipse.setFillColor(LIGHT_YELLOW);
        ellipse.setOutlineThickness(15.f);
        ellipse.setOutlineColor(BROWN);
        ellipse.setPosition(sf::Vector2f(170.f, 250.f));
        window.draw(ellipse);

        //--------------- ear
        sf::CircleShape ucho(15.f);
        ucho.setPosition(sf::Vector2f(310.f, 75.f));
        ucho.setFillColor(LIGHT_YELLOW);
        ucho.setOutlineThickness(15.f);
        ucho.setOutlineColor(BROWN);
        window.draw(ucho);

        //---------------- head
        ellipse.setRadius(15.f);
        ellipse.setScale (6,5);
        ellipse.setFillColor(BROWN);
        ellipse.setOutlineThickness(0.2f);
        ellipse.setOutlineColor(sf::Color::Black);
        ellipse.setPosition(sf::Vector2f(160.f, 80.f));
        window.draw(ellipse);

        //---------------- face
        triangle[0].color = triangle[1].color = triangle[2].color = sf::Color::Black;
        triangle[0].position = sf::Vector2f(235.f, 185.f);
        triangle[1].position = sf::Vector2f(265.f, 185.f);
        triangle[2].position = sf::Vector2f(250.f, 205.f);
        window.draw(triangle);

        circle.setRadius(20.f);
        circle.setFillColor(WHITE);
        circle.setPosition(sf::Vector2f(190.f, 135.f));
        window.draw(circle);

        circle.setPosition(sf::Vector2f(270.f, 135.f));
        window.draw(circle);

        circle.setRadius(10.f);
        circle.setFillColor(sf::Color::Black);
        circle.setPosition(sf::Vector2f(208.f, 135.f));
        window.draw(circle);

        circle.setPosition(sf::Vector2f(288.f, 135.f));
        window.draw(circle);

        //---------------- hat
        triangle[0].color = triangle[1].color = triangle[2].color = RED_WINE;
        triangle[0].position = sf::Vector2f(130.f, 30.f);
        triangle[1].position = sf::Vector2f(250.f, 80.f);
        triangle[2].position = sf::Vector2f(160.f, 140.f);
        window.draw(triangle);

        circle.setRadius(18.f);
        circle.setFillColor(WHITE);
        circle.setPosition(sf::Vector2f(120.f, 25.f));
        window.draw(circle);

        line.setFillColor(WHITE);
        line.setPosition (sf::Vector2f(252.f,79.f));
        line.setSize(sf::Vector2f(116.f,8.f));
        line.setRotation(146.f);
        window.draw (line);

        //---------------- bear's present
        rectangle.setSize(sf::Vector2f(260.f, 150.f));
        rectangle.setPosition (86.f,340.f);
        rectangle.setFillColor(GRAY);
        rectangle.setOutlineThickness(1.f);
        rectangle.setOutlineColor(GRAY2);
        window.draw(rectangle);

        convex.setPoint(0, sf::Vector2f(86.f, 340.f));
        convex.setPoint(1, sf::Vector2f(126.f, 300.f));
        convex.setPoint(2, sf::Vector2f(386.f, 300.f));
        convex.setPoint(3, sf::Vector2f(346.f, 340.f));
        convex.setFillColor(GRAY);
        convex.setOutlineThickness(1.f);
        convex.setOutlineColor(GRAY2);
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(346.f, 340.f));
        convex.setPoint(1, sf::Vector2f(386.f, 300.f));
        convex.setPoint(2, sf::Vector2f(386.f, 450.f));
        convex.setPoint(3, sf::Vector2f(346.f, 490.f));
        convex.setFillColor(GRAY);
        convex.setOutlineThickness(1.f);
        convex.setOutlineColor(GRAY2);
        window.draw(convex);

        //---------------- bear's gift ribbon
        line.setFillColor(RED_WINE);
        line.setPosition (sf::Vector2f(236.f,340.f));
        line.setSize(sf::Vector2f(152.f,20.f));
        line.setRotation(90.f);
        window.draw (line);

        convex.setPoint(0, sf::Vector2f(216.f, 340.f));
        convex.setPoint(1, sf::Vector2f(256.f, 300.f));
        convex.setPoint(2, sf::Vector2f(276.f, 300.f));
        convex.setPoint(3, sf::Vector2f(236.f, 340.f));
        convex.setFillColor(RED_WINE);
        convex.setOutlineThickness(0.f);
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(100.f, 322.f));
        convex.setPoint(1, sf::Vector2f(110.f, 312.f));
        convex.setPoint(2, sf::Vector2f(374.f, 312.f));
        convex.setPoint(3, sf::Vector2f(364.f, 322.f));
        window.draw(convex);

        convex.setPoint(0, sf::Vector2f(364.f, 322.f));
        convex.setPoint(1, sf::Vector2f(374.f, 312.f));
        convex.setPoint(2, sf::Vector2f(374.f, 462.f));
        convex.setPoint(3, sf::Vector2f(364.f, 472.f));
        window.draw(convex);

        //--------------- left arm
        ellipse.setRadius(7.f);
        ellipse.setScale(3,4);
        ellipse.setFillColor(BROWN);
        ellipse.setOutlineThickness(0.2f);
        ellipse.setOutlineColor(sf::Color::Black);
        ellipse.setPosition(sf::Vector2f(74.f, 350.f));
        window.draw(ellipse);

        //--------------- right arm
        ellipse.setRadius(7.f);
        ellipse.setScale(3,4);
        ellipse.setFillColor(BROWN);
        ellipse.setOutlineThickness(0.2f);
        ellipse.setOutlineColor(sf::Color::Black);
        ellipse.setPosition(sf::Vector2f(380.f, 310.f));
        window.draw(ellipse);

        //--------------- left foot
        ellipse.setRadius(13.f);
        ellipse.setScale(3,4);
        ellipse.setFillColor(BROWN);
        ellipse.setOutlineThickness(0.2f);
        ellipse.setOutlineColor(sf::Color::Black);
        ellipse.setPosition(sf::Vector2f(63.f, 440.f));
        window.draw(ellipse);

        //----------------- right foot
        ellipse.setRadius(13.f);
        ellipse.setScale(3,4);
        ellipse.setFillColor(BROWN);
        ellipse.setOutlineThickness(0.2f);
        ellipse.setOutlineColor(sf::Color::Black);
        ellipse.setPosition(sf::Vector2f(333.f, 440.f));
        window.draw(ellipse);

        //wishes
        text.setFillColor(LIGHT_YELLOW);
        text.setCharacterSize(40.f);
        text.setPosition(355, 60);
        text.setString("    Merry Christmas\n and a happy New Year!");
        window.draw(text);


        // end the current frame
        window.display();
    }
    return 0;
}